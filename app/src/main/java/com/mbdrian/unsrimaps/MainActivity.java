package com.mbdrian.unsrimaps;

import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.FloatMath;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Spinner sp;
    ImageMap mImageMap;
    String name;
    String desk;
    String foto;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sp = (Spinner) findViewById(R.id.spinner);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                Toast.makeText(getBaseContext(), sp.getSelectedItem().toString(),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // find the image map in the view
        mImageMap = (ImageMap)findViewById(R.id.map);
        mImageMap.setImageResource(R.drawable.map_fakultas1);

        // add a click handler to react when areas are tapped
        mImageMap.addOnImageMapClickedHandler(new ImageMap.OnImageMapClickedHandler() {
            @Override
            public void onImageMapClicked(int id, ImageMap imageMap) {
                // when the area is tapped, show the name in a
                // text bubble
                name = mImageMap.showBubble(id);
                String[] parts = name.split("-");
                name = parts[0];
                desk = parts[1];
                foto = parts[2];
            }

            @Override
            public void onBubbleClicked(int id) {
                //Toast.makeText(getApplicationContext(), ""+id, Toast.LENGTH_SHORT).show();
                Intent intObj = new Intent(MainActivity.this,DetailPeta.class);
                intObj.putExtra("id_fakultas", id);
                intObj.putExtra("nama_fakutlas",name);
                intObj.putExtra("deskripsi_fakultas",desk);
                intObj.putExtra("foto_fakultas",foto);
                startActivity(intObj);
            }

            @Override
            public void onBubbleClicked(int id, ImageMap imageMap) {
                Toast.makeText(getApplicationContext(), imageMap.getName(id), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
