package com.mbdrian.unsrimaps;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailPeta extends AppCompatActivity {

    TextView judul;
    TextView deskripsi;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_peta);

        judul = (TextView)findViewById(R.id.textView);
        deskripsi = (TextView) findViewById(R.id.textView2);
        imageView = (ImageView) findViewById(R.id.imageView);

        Intent intename = getIntent();

        int judulFak = intename.getIntExtra("id_fakultas", 0);
        String judulreal = intename.getStringExtra("nama_fakutlas");
        String deskripsiFak = intename.getStringExtra("deskripsi_fakultas");
        String foto = intename.getStringExtra("foto_fakultas");

        judul.setText(""+judulreal);
        deskripsi.setText(deskripsiFak);
        int idgambar = getResources().getIdentifier(foto, "drawable", getPackageName());
        imageView.setImageResource(idgambar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

}
